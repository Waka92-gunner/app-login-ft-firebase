// login
var provider = new firebase.auth.GoogleAuthProvider();

$('#login').click(function(){
    firebase.auth()
  .signInWithPopup(provider)
  .then((result) => {
      console.log(result.user);
    /** @type {firebase.auth.OAuthCredential} */
    var credential = result.credential;

    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    // ...
  });
});